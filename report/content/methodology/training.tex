\subsection{Training}
\label{subsec:training}
After presenting the generic algorithm \ref{sec: algorithm}, it can be understood that two main stages have to take place, training and testing. The former will extract the phonemes from the sample data classifying them into a dictionary. The latter will process the testing sample to compare its phonemes with the phoneme dictionary.

In this section of the report we will study the training stage, which consists of a common part for the sample processing, and three different approaches for the codebook-building section of the algorithm.

Regarding the common component, all the samples will be processed the same way. First of all, MATLAB reads the sample and the signal is taken trough a filtering stage. Here, the higher frequencies are emphasized using the pre-emphasis filter, and the signal is smoothed using a Savitzky–Golay filter. We also found important to remove the noise from the silent fragments of the samples, thus a moving average filter kernel is also applied to remove this DC component and get well defined characteristics in the time domain.

After the signal has been filtered, we compute the first 14 MFCC coefficients. It is recommended for speech recognition that the coefficient liftering is applied to give less weight to higher coefficients. Thus a sinusoidal lifter is used in the coefficients. Finally we extract the possible phonemes using \emph{k-means} clustering. 

We carried out the variations in the training by changing the way we treated the phonemes of the samples. Three different methods were studied.

\begin{enumerate}
  \item Increasing the dictionary entries dynamically: Adding new phonemes to the codebook if it does not exist yet. Based on the distance between phoneme vectors.
  \item Bulk k-means clustering: Performing k-means clustering in each of the samples, save all cluster centers, and perform k-means again to get the final clusters.
  \item Dictionary based on average cluster centers: Obtain the final dictionary by averaging the cluster centers of each number, and clustering again to get all of the phonemes.
\end{enumerate}

\subsubsection{Approach 1: Increase dictionary dynamically}
\label{subsubsec:training-approach1}
In this first method, we implemented a dynamic increasing of the phoneme codebook based on the distance value. The result will not be limited by the number of real phonemes, meaning that the algorithm will output the number of phonemes that it decides according to the difference between them. 

In the dynamic creation of the codebook, we analyze one sample at a time. For each iteration we have that first, the sample is read, pre-processed using the pre-emphasis filter, and its MFCC coefficients are computed. Then, k-means clustering is performed to extract the possible phonemes in the word. The number of clusters depends on the digit, so \emph{zero} would have four phoneme clusters and one silent cluster and \emph{jeden} would have five phoneme clusters and one silent cluster. These clusters will be compared individually to the codebook using vector quantization, and the error will be computed. This error will determine whether we have found a new phoneme or whether it already exists. The threshold is set manually to as a reasonable value of 250 after observing the list of possible errors. If a new phoneme has been detected in the loop, then it must be added to the dictionary. Otherwise it will be ignored. 

The final result of the codebook will be a list of all the phonemes detected by the algorithm. Its length will depend on the threshold and on how well it performs. In theory, according to \ref{fig:polish-numbers-phonemes} the total number of phonemes in the number samples should be 24, however depending on the pronunciation, silence and environment sound the number of clusters might be greater.

\subsubsection{Approach 2: Clustering of all samples clusters}
\label{subsubsec:training-approach2}
After testing the dynamic dictionary increasing approach, we continued testing on the algorithm by performing double k-means clustering. Theoretically, by forcing the algorithm to have 25 clusters (24 for the phonemes and one for the silence), the result should be a dictionary containing these. The procedure of obtaining the codebook is as follows.

In a loop that goes over all of the samples, we obtain the cluster centers of the MFCCs for each of them. We will store this in a training matrix that will increase its size, containing all the possible phonemes for all samples. 

When all the samples have been analyzed, we carry out clustering of the training matrix, that should contain all possible phonemes. The number of clusters, as it has been mentioned, should be 25. 

The resulting codebook in theory, should contain the possible phonemes contained in the training samples. Moreover, similar phonemes from different samples might be grouped together due to this enforcing of 25 cluster centers.

\subsubsection{Approach 3:  Dictionary based on average cluster centers}
\label{subsubsec:training-approach3}
We performed a final approach based in the idea of creating the codebook by working on each of the numbers separately and finally combining the results.

In a more detailed way, we read all the samples for each digit. So for number \emph{zero} we read all four corresponding training samples. Then, we compute the k-means clustering for each of them to get the possible phonemes. In the case of \emph{zero}, this would lead us to have four samples, with five cluster centers in each (one corresponding to silence and the other four to the phonemes). The next step is to match these phonemes in between samples. So phoneme \emph{z} should have similar value in all samples for number \emph{zero}, phoneme \emph{e} as well, and so on, until all phonemes are matched. 

After this matching, we average the results of all the coinciding phonemes to get a more generic cluster center. Going back to our example, all cluster centers corresponding to \emph{z} from all training samples will be averaged. 

The averaging will result in the phonemes corresponding to each number. Then, performing a 25 center k-means clustering on them should result in grouping the repeated phonemes, as well as the ones corresponding to silences. The output is the final codebook, with the total number of phonemes in the numbers and the silent phoneme. 
 