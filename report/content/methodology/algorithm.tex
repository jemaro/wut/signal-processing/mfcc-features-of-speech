\def\imgpath{content/methodology/algorithm/images}
\subsection{Algorithm description} \label{sec: algorithm}

\begin{figure}
    \caption{Algorithm overview}
    \input{content/methodology/algorithm/diagram.tex}
    \label{fig:alg-overview}
\end{figure}

In order to prepare our data, we need to divide our audio recordings into two
subsets. One will be used for training and the other for testing. The audios
for the training subset should contain samples from all the numbers and with
different pitches. 

The main purpose for the training is to build a dictionary that relates
phonemes and MFCC vectors. This dictionary will be used for the identification
of the speech in the testing as shown in \autoref{fig:alg-overview}.

\subsubsection{Pre-emphasis}
\label{subsec:pre-emphasis}
We usually need to do a pre-processing of the data before feeding it to the
MFCC machine described in \autoref{fig:mfcc-algorithm}. It is convenient to take the signal through a filtering stage. We señected three different filters. The first one is the
\emph{pre-emphasis filter}, which will emphasize the higher
frequencies of the speech signal. Its purpose is to balance the spectrum of voiced sounds that have a steep roll-off in the high-frequency region. A common way to achieve this for
a signal $X$ composed of $N$ samples is described in
\autoref{eqn:pre-emphasis}.

\begin{equation}
    \label{eqn:pre-emphasis}
    X_n' = X_n - \phi*X_{n-1}
    \text{ where }\phi \in [0.9, 1.0]
    \text{ and } n \in [0,N-1]
\end{equation}

The second filter type that proved to work well for smoothing the signal is the \emph{Savitzky-Golay} filter. This is a finite impulse response (FIR) filter kernel that will make the signal smoother. It can be easily implemented in MATLAB and its working principle can be found in \cite{Schafer2011}. 

The last filter was intended to reduce part of the noise during the silent frames of the samples. We added a \emph{moving average filter} for this purpose. 

\subsubsection{Windowing}
\label{subsec:windowing}
Next, we perform the windowing, which means dividing the signal into small
parts so that the speech signal is represented as a sequence of feature vectors
in time. This step might be considered inside \nameref{subsec:mfcc} but as it
is crucial for the \nameref{subsec:identification} it is described apart.

The reason behind windowing the signal is that the speech is a slowly
time-varying or quasi-stationary signal. For stable acoustic characteristics,
speech needs to be examined over a sufficiently short period of time.
Therefore, speech analysis must always be carried out on short segments across
which the speech signal is assumed to be stationary. The typical duration of
the windows used in speech analysis is around 20ms and advanced every 10ms.
Thus resulting in parts of the signal being present in 2 consecutive windows,
the purpose of the overlapping is that each speech sound of the input sequence
would be approximately centered at some frame \cite{Springer2014}.

\subsubsection{MFCC Feature Extraction}
\label{subsec:mfcc}
The Mel Frequency Cepstrum Coefficients extraction method is a combination of
different techniques that need to be executed in a certain order in order to
achieve the wanted result as illustrated in \autoref{fig:mfcc-algorithm}. 

\begin{figure}
    \caption{Basic steps for the MFCC calculation}
    \includegraphics[width=\textwidth]{\imgpath/mfcc-algorithm.png}
    \label{fig:mfcc-algorithm}
\end{figure}

\subsubsubsection{Discrete Fourier Transform}

We have already described the \nameref{subsec:windowing}. The step that comes
after is performing the Discrete Fourier Transform on each windowed frame
and getting the short time magnitude spectrum as described in \autoref{eqn:dft}
where N is the number of points used to compute the DFT.

\begin{equation}
    \label{eqn:dft}
    X(k)=\sum_{n = 0}^{N-1} x(n) e^{{-j 2 \pi n k}/{N}};
    \qquad 0 \leq k \leq N-1
\end{equation}

This computation can be reduced by making use of the Short Time Fourier
Transform algorithm. \autoref{eqn:stft} returns the Fourier coefficients for frame
$\tau$ and frequency index $k$ where $M$ is the size of the window.

\begin{equation}
    \label{eqn:stft}
    F(k,\tau)=\sum_{t=0}^{M-1}[x[\tau+t] e^{{-j 2 \pi k t}/{M}} w_\tau[t]];
    \qquad k=0, 1, ..., M-1
\end{equation}

\subsubsubsection{Mel Feature Coefficients}
\label{subsubsubsec:mel-feature-coefficients}
After calculating $F(k,\tau)$, we will compute the \emph{Mel Feature
Coefficients}, or \emph{Mel Spectral Features}. In the first place it is
necessary to adjust the frequencies of the signal to the Mel scale, which
imitates the nonlinearity of the human ear to frequency components in the audio
spectrum. This scale is described by \autoref{eqn:fmel}.

\begin{equation}
    \label{eqn:fmel}
    f_{Mel} = 2595 \log_{10} \left( 1+\frac{f}{700} \right)
\end{equation}

In the second place, we need to filter the signal obtained from the DFT using
filters uniformly located in the Mel frequency scale. The most common ones are
triangular filters, called the Mel Filter Bank (\autoref{fig:mel-fliterbank}).
Each of the triangular filters will result in one Mel-spectral coefficient.
Generally, between 12 and 38 coefficients are needed to represent the most
meaningful characteristics of the signal.

\begin{figure}
    \caption{
        Mel filter bank with triangular filters spaced linearly at first and
        then logarithmically
    }
    \centering
    \includegraphics[width=0.7\textwidth]{\imgpath/mel-filterbank.png}
    \label{fig:mel-fliterbank}
\end{figure}

The MFCs are computed by multiplying in the frequency domain (or convolving in
the time domain) the Fourier coefficients with each of the triangular filters
and adding the up. This is described in \autoref{eqn:mfc}. Where $X(k)$ is the
magnitude spectrum obtained from the DFT corresponding to the the $k$th window,
and $H_m(k)$ is the weight given to the kth energy spectrum bin contributing to
the $m$th output band. The value of $H_m(k)$ is given by \autoref{eqn:Hm}.

\begin{equation}
    s(m) = \sum_{k=0}^{N-1}[|X(k)|^2 H_m(k)];\qquad 0\leq m\leq M - 1
    \label{eqn:mfc}
\end{equation}

\begin{equation}
    H_m(k) = 
    \begin{cases} 
        0, & k \leq f(m-1) \\
        \frac{2(k-f(m-1))}{f(m)-f(m-1)} & f(m-1) \leq k \leq f(m) \\
        \frac{2(f(m+1)-k)}{f(m+1)-f(m)} & f(m) \le k \leq f(m+1) \\
        0 & k \ge f(m+1)
    \end{cases}
    \label{eqn:Hm}
\end{equation}

\subsubsubsection{Mel Frequency Cepstrum Coefficients}
The obtained Mel spectrum is represented in the logarithmic scale and the
cepstral coefficients are obtained using the Discrete Cosine Transform (DCT),
given by \autoref{eqn:dct}.

\begin{equation}
    c(n)=\sum_{m=0}^{M-1} \log_{10}(s(m))cos(\frac{\pi n (m-0.5)}{M});
    \qquad n = 0, 1, 2, ..., C-1
    \label{eqn:dct}
\end{equation}

The result will be each of the MFCC features of the signal. The steps of the
MFCC feature computation are performed in MATLAB using the Audio Processing
Toolbox by selecting the desired type of window and the
\href{https://www.mathworks.com/help/audio/ref/mfcc.html}{mffc} integrated
function. This will return a matrix with the number of rows equal to the number
of frames N and the number of columns equal to the number of features M. By
default, the function computes 14 features which results in N vectors of 14
dimensions.

Moreover, we added a liftering stage as proposed in \cite{Lifter2019} in order to give less weight to higher coefficients and obtain better results for speech recognition. The filter equation is given by \ref{eqn:lifter}. 
\begin{equation}
    MFCC_{filtered_i} = w_iMFCC_{unfiltered_i}
\end{equation}
\begin{equation}
    w_i = 1 + \frac{D}{2} sin(\frac{\pi i}{D})
    \label{eqn:lifter}
\end{equation}
The value of D corresponds to the dimensions of the filter, which is equal to the number of MFCC coefficients.

Furthermore, due to the increased dimensions of the feature vectors, the data can’t be visualized directly. For
this purpose, the dimensionality can be reduced using the
\nameref{subsubsec:t-SNE} (t-SNE) algorithm.

\subsubsubsection{t-Distributed Stochastic Neighbour Embedding}
\label{subsubsec:t-SNE}

The t-SNE algorithm is a nonlinear dimensionality reduction technique used for
visualization of high dimensional data in lower dimensions. The algorithm has
two stages in which it defines probability distributions around the data
points.

The first part will measure the density of all points under a Gaussian
distribution such that similar samples will be assigned a similar probability
while more different samples will be assigned lower probability.

The second part will perform a similar transformation using a different
distribution, called Cauchy distribution. This will take the samples to a lower
dimensional map. After this, the two probability distributions are subtracted
and a loss function is minimized to determine the location of the points in the
2D map.

This technique will be used to visualize the clustering results in the same way
that is shown in \autoref{fig:alg-overview}.

\subsubsection{K-means clustering}
\label{subsec:k-means}
After all the MFCC features have been computed, unlabelled data can be trained
to obtain some basic patterns and label them to be able to identify the words
corresponding to the numbers. K-means clustering with Vector Quantization will
be used with this purpose.

K-means clustering is an unsupervised learning algorithm used to group similar
data points together based on certain similarities and discover new patterns.
After extracting the MFCC feature vectors, they need to be compared. Similar
vectors will represent similar phonemes, which will be assigned a centroid by
the k-means clustering algorithm. For example, the word \emph{dwa} will have
three phonemes as shown \autoref{fig:polish-numbers-phonemes}. That will lead
to one cluster for each phoneme, and another cluster for the silence in the
rest of the audio signal.

\begin{figure}
    \caption{Polish numbers phonetic transcription}
    \centering
    \includegraphics[width=0.5\textwidth]{\imgpath/polish-numbers-phonemes.png}
    \label{fig:polish-numbers-phonemes}
\end{figure}

\subsubsection{Identification}
\label{subsec:identification}
As a final step the different signal windows need to be assigned to one of the
clusters. This step is trivial in the training phase as the clustering
operation indexes every window to a cluster. But the testing phase does not
apply the clustering algorithm as shown in \autoref{fig:alg-overview}. Instead
the window frames are identified from the dictionary of phonemes developed from
in the testing phase.

Vector Quantization will simplify the representation of the word in an
efficient manner by mapping vectors from a large vector space to a finite
number of regions (clusters) in that space. The centroids of the clusters
obtained from k-means will be called codewords. The collection of codewords
will be called a codebook, or dictionary. The training phase will result in a
dictionary of all the phonemes contained in all the numbers going from 0 to 10.

The testing phase will be carried out by comparing an input signal to all
the codewords in the dictionary. The output will be a sequence of phonemes that
represent the word.
