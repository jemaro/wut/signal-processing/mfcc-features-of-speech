\section{Implementation}

Algorithms and methods described in \autoref{sec:methodology} have been
implemented using \href{https://www.mathworks.com/products/matlab.html}{MATLAB
programming language}. Together with some auxiliary and debug functions.

Collaborative work and version control was ensured by using a
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech}{cloud
code repository}.

\subsection{Dataset}
\label{subsec:dataset}
The given dataset for the development of the project contains only two sets of
samples of the pronunciation of the numbers 0 to 9. This means that we are
given only two audio signals for each number, one from a male and other from a
female voice.

This dataset has been augmented with additional samples and split for test
and training. 

\subsubsection{Augmented dataset}
Being twenty the total given number of samples very small, we decided to
augment it by asking some polish speakers. We added three more sample sets, two
from female voices and one from a male voice. Reaching a total of fifty
samples. More than double than the originally given.

\subsubsection{Naming convention}
The
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/tree/master/allsamples}{dataset}
samples have been renamed using the following convention:
\texttt{[\emph{sample\_number}][\emph{gender}][\emph{digit}].wav}. Being
\emph{sample\_number} a numerical identifier of the sample set, from \emph{1}
to \emph{5} with our augmented dataset; \emph{gender} the type of the voice
found in the signal, \emph{m} for male and \emph{f} for female; \emph{digit}
the number that is pronounced in the particular signal, from \emph{0} to
\emph{9} with our dataset.

\subsubsection{Training data}
\label{subsubsec:training-data}
To ensure proper results on \nameref{subsec:testing}, some samples can't be
used during the \nameref{subsec:training}. We define the sample sets from 1 to
4 to be used for training, and the sample set 5 to be used only for testing. In
this way, we have a total of 40 audio samples used in training and 10 used only
in testing.

\nameref{subsec:training} implementation will use this definition as default,
but it can be overridden when calling the function in order to use a different
training set of samples.

\subsection{Pre-emphasis}
We implemented the algorithm described in \autoref{subsec:pre-emphasis} in
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/preEmphasis.m}{\texttt{preEmphasis}}.
This function will be used in both, training and testing, as well as the
implementations described in \autoref{subsec:mfcc} and
\autoref{subsec:impl-kmeans}.

\subsection{MFCC Feature Extraction}
\label{subsec:impl-mfcc}
We implemented the function
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/computeMFCC.m}{\texttt{computeMFCC}}
that returns the MFCC coefficients of a given signal given its sampling rate,
the number of coefficients of to extract per frame and the length in signal
samples of each frame.

\subsection{K-means clustering}
\label{subsec:impl-kmeans}
We also implemented the function
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/computeKmeans.m}{\texttt{computeKmeans}}
which wraps the built in MATLAB function
\href{https://www.mathworks.com/help/stats/kmeans.html}{\texttt{kmeans}} with our tuned
parameters, making it more accessible across the project.

\subsection{Training}
\label{subsec:impl-training}
The training algorithm described in \autoref{subsec:training} is implemented in
the function
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/train.m}{\texttt{test}}.
It should be noted that this function has four optional inputs, being the first
two the digits and the sample numbers to be used for training, which define the
training audio signals. Defaults to the training data described in
\autoref{subsubsec:training-data}.

The third input is the method to be used, being
\texttt{accumulateIfNew} the default option which corresponds to
\nameref{subsubsec:training-approach1}. \texttt{accumulateAllAndCluster} and
\texttt{accumulateDigitsAndCluster} correspond to
\nameref{subsubsec:training-approach2} and
\nameref{subsubsec:training-approach3} respectively.

Finally, the fourth input is a boolean specifying if debug mode should be used.
This mode is not used by default, but it is handy to debug the function and
dive deep into the training algorithm by plotting intermediate results and
stopping at key points of the training.

This function returns a
\href{https://www.mathworks.com/help/dsp/ref/dsp.vectorquantizerencoder-system-object.html#d122e325162}{vector
quantizer encoder System object} named \texttt{getPhoneme} and the phonemes
dictionary that acts as a
\href{https://www.mathworks.com/help/dsp/ref/dsp.vectorquantizerencoder-system-object.html#d122e325804}{codebook}
named \texttt{phonemes}. This phonemes dictionary can be used then with
\texttt{getPhoneme} together with another input vector of
\nameref{subsubsubsec:mel-feature-coefficients} in order to obtain the closest
phoneme.

Additionally, both outputs are saved as workspace variables no matter how
is the function called. This allows an easier interaction with the \texttt{train}
function described in \autoref{subsec:impl-testing}.

\subsection{Testing}
\label{subsec:impl-testing}
As described in \autoref{subsec:testing}, audio signals corresponding to the
pronunciation of the same digit should lead to similar sequences of phonemes.
Function
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/test.m}{\texttt{test}}
allows us to visualize those sequences of phonemes of a given signal.

The signals to be tested are determined by the two inputs of the
function: \emph{digit} and \emph{sample\_numbers}. The first one determines
the digit that will be pronounced in the audio signal, and the second one
defines which samples will be used from our \nameref{subsec:dataset}. It is
possible to test signals used for the training.

This function will attempt to use the \emph{getPhoneme} and \emph{phonemes}
variables in the workspace. These are described in
\autoref{subsec:impl-training}. If those variables can't be find in the
workspace, default ones will be loaded from the file
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/phonemes.mat}{\texttt{phonemes.mat}}.

After processing the signals, they are plotted in an interactive graph using
the utility described in \autoref{subsubsec:impl-plotaudio} together with the
obtained phoneme sequence highlighted in different colors over the playable
signal.

\subsection{Main}
As a demonstration of the project, we have developed several scenarios that
showcase different phoneme sequences and phoneme dictionaries created with
the approaches described in \autoref{subsec:training}.

These scenarios can be found in the root of the
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech}{code
repository} together with the function
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/main.m}{\texttt{main}}
that shows a prompt with the descriptions of the three scenarios,
\nameref{subsec:impl-training} and \nameref{subsec:impl-testing}, allowing the
user to select an operation to run. If the project should be demonstrated using
only one function it would be this one.

\subsection{Auxiliary functions}
\label{subsec:impl-auxiliary}
We have developed many auxiliary functions, some of them are not used in the
final implementation as they were only useful for debugging. Here we highlight
two of them because of their relevance in the final implementation.

\subsubsection{Plot audio}
\label{subsubsec:impl-plotaudio}
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/plotaudio.m}{\texttt{plotaudio}}
is an auxiliary function based on Lukasz Laszko
\href{https://www.mathworks.com/matlabcentral/fileexchange/73795-audioplayer-gui-with-a-v-sync}{AudioPlayer
GUI} that allows to plot a signal while offering control to play it as a whole
or some part of it. Unlike Lukasz's version, this function allows to plot
several signals in the same figure and play them independently. 

This can be done due to the fact that the
\href{https://www.mathworks.com/help/matlab/ref/audioplayer.html}{\texttt{audioplayer}}
object is attached to an axes object, being the play controls available as
\href{https://www.mathworks.com/help/matlab/ref/axtoolbarbtn.html}{axes toolbar
buttons} as it can be seen in \autoref{fig:plotaudio}.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{\subdir/plotaudio.png}
    \caption{MATLAB's sample audio file \emph{handel.mat} plotted using the
    auxiliary function \texttt{plotaudio}. Green cursor shows the location of
    the played signal. Yellow cursors limit the audio signal to be played.}
    \label{fig:plotaudio}
\end{figure}

\subsubsection{Read sample}
\label{subsubsec:impl-readsample}
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/readsample.m}{\texttt{readsample}}
is an auxiliary function that allows to easily read a an audio sample from our
\nameref{subsec:dataset} given the digit that it is pronounced in it and the
index of our samples library. It makes use of another of our auxiliary
functions:
\href{https://gitlab.com/jemaro/wut/signal-processing/mfcc-features-of-speech/-/blob/master/lib/audiofiles.m}{\texttt{audiofiles}}.
A wrapper of MATLAB's built in
\href{https://www.mathworks.com/help/matlab/ref/dir.html}{\texttt{dir}} function that
returns a cell array of file paths given a folder and a wildcard.