function [getPhoneme, phonemes] = train(digits, sample_numbers, method, debug)
% TRAIN Builds a dictionary of phonemes out of a determined set of audio
% samples using different methods.
%
%   [getPhoneme, phonemes] = TRAIN() Trains with the default training dataset,
%   the "accumulateIfNew" method and no debug mode.
%
%   [getPhoneme, phonemes] = TRAIN(digits, sample_numbers) Trains with the set
%   of audio samples determined by digits and sample_numbers, the
%   "accumulateIfNew" method and no debug mode.
%
%   [getPhoneme, phonemes] = TRAIN(digits, sample_numbers, method) Trains with
%   the set of audio samples determined by digits and sample_numbers, a
%   determined method and no debug mode.
%
%   [getPhoneme, phonemes] = TRAIN(digits, sample_numbers, method, debug)
%   Trains with a determined set of audio samples, a determined method, and if
%   debug is true, the debug mode will be activated.
%
% INPUT
%
% digits:           Vector of digits pronounced in the audio samples. Defaults
%                   to [0:9], being all digits in the dataset.
%
% sample_numbers:   Vector of sample sets to use as training audio samples.
%                   Defaults to [1:4], being the 4 first samples out of 5 from
%                   the dataset.
%
% method:           Method to use for training. Defaults to "accumulateIfNew".
%                   Other options are "accumulateAllAndCluster" and
%                   "accumulateDigitsAndCluster"
%
% debug:            Boolean that defines wether to use debug mode or not. Being
%                   true the value for using debug mode. In this mode, every
%                   training iteration, the phonemes dictionary is ploted and
%                   the execution is paused.
%
% OUTPUT
%
% getPhoneme:      Vector quantizer encoder object to be used together with
%                  "phonemes" as codebook.
% 
% phonemes:        Codebook to be used with getPhoneme. Behaves as a phoneme
%                  dictionary of the training set of audio signals. Different
%                  phonemes are stored in columns, being the rows their MFCC
%                  coefficients. 
% 
% See also PROCESSSAMPLE, MFCC, DSP.VECTORQUANTIZERENCODER

%% Initialization
% Optional inputs
if ~exist('digits', 'var')
    digits = [0:9];
end
if ~exist('debug', 'var')
    sample_numbers = [1:4];
end
if ~exist('debug', 'var')
    debug = false;
end
if ~exist('method', 'var')
    method = 'accumulateIfNew';
end
addpath('lib'); % Add library
% Common method parameters
N_coeffs = 14;  % Number of MFC coefficients 
phi = 0.95;
extra_clusters = 2; % Number of clusters to add to the real number of phonems 
                    % of a digit
frame_duration = 0.08; % seconds
% Build the vector quantizer object
getPhoneme = dsp.VectorQuantizerEncoder(...
    'CodebookSource', 'Input port', ...
    'CodewordOutputPort', true, ...
    'QuantizationErrorOutputPort', true ...
    );

%% Body
% Call the defined method
phonemes = feval(method, digits, sample_numbers, debug, ... 
                 getPhoneme, N_coeffs, phi, extra_clusters, frame_duration);
% Sort and plot the output
phonemes = plotPhonemesSorted(phonemes, method);
% Save the output in the base workspace
assignin('base', 'phonemes', phonemes); 
assignin('base', 'getPhoneme', getPhoneme);
end

function phonemes = accumulateIfNew(digits, sample_numbers, debug, ... 
    getPhoneme, N_coeffs, phi, extra_clusters, frame_duration)
    % ACCUMULATEIFNEW Builds the phonemes dictionary by accumulating phonems
    % only when they are different enough from the rest of phonems in the
    % dictionary 

    % Parameters
    errThreshold = 170;

    for digit = digits
        for sample_number = sample_numbers
            C = processSample(digit, sample_number, N_coeffs, phi, ... 
                              extra_clusters, frame_duration);
            if ~exist('phonemes', 'var')
                % If is the first iteration, we assign the phonemes as the clusters
                phonemes = C';
                if debug
                    plotPhonemes(phonemes)
                    keyboard;
                end
            else
                % For each cluster in C, check if is a phonem we have
                for n=[2:size(C,1)-1]
                    possible_phonem = C(n,:)';
                    release(getPhoneme) % Avoid error using VectorQuantizerEncoder
                    [index, phoneme, err] = getPhoneme(possible_phonem, phonemes);
                    index = index + 1; % Correct "index" that starts with 0 
                    % If the quantization error is too high, add it as a new phonem
                    if err > errThreshold
                        phonemes(:,end+1)=possible_phonem;
                    else
                        phonemes(:,index)= (phoneme+possible_phonem)./2 ;
                    end
                    if debug
                        fprintf('Digit %d Sample %d Err %f\n', ... 
                            digit, sample_number, err)
                        plotPhonemes(phonemes)
                        keyboard;
                    end
                end
            end
        end
    end
end

function phonemes = accumulateAllAndCluster(digits, sample_numbers, debug, ... 
    getPhoneme, N_coeffs, phi, extra_clusters, frame_duration)
    % ACCUMULATEALLANDCLUSTER Builds the phonemes dictionary by accumulating
    % all posible phonems and applying Kmeans clustering afterwards knowing the
    % total number of phonemes that should be in the dictionary

    % Parameters
    N_phonemes = 26;

    for digit = digits
        for sample_number = sample_numbers
            C = processSample(digit, sample_number, N_coeffs, phi, ... 
                              extra_clusters, frame_duration);
            if ~exist('phonemes', 'var')
                % If is the first iteration, we assign the phonemes as the clusters
                phonemes = C;
            else
                phonemes = [phonemes; C];
            end
            if debug
                plotPhonemes(phonemes')
                keyboard;
            end
        end
    end
    [~, phonemes] = computeKmeans(phonemes, N_phonemes);
    phonemes = phonemes';
end

function phonemes = accumulateDigitsAndCluster(digits, sample_numbers, ... 
    debug, getPhoneme, N_coeffs, phi, extra_clusters, frame_duration)
    % ACCUMULATEALLANDCLUSTER Builds the phonemes dictionary by accumulating
    % posible phonems of different digits and applying Kmeans clustering
    % afterwards knowing the total number of phonemes that should be in the
    % dictionary.

    % Parameters
    N_phonemes = 27;

    phonemes = [];
    for digit = digits
        digit_phonemes = [];
        for sample_number = sample_numbers
            C = processSample(digit, sample_number, N_coeffs, phi, ... 
                              extra_clusters, frame_duration);
            if isempty(digit_phonemes)
                % If is the first iteration, we assign the phonemes as the clusters
                digit_phonemes = C';
                if debug
                    plotPhonemes(phonemes)
                    keyboard;
                end
            else
                % All our phonemes in C should be repeated so let's merge them
                % with digit_phonemes
                for n=[1:size(C,1)]
                    possible_phonem = C(n,:)';
                    release(getPhoneme) % Avoid error using VectorQuantizerEncoder
                    [index, phoneme, ~] = getPhoneme(possible_phonem, digit_phonemes);
                    index = index + 1; % Correct "index" that starts with 0 
                    phonemes(:,index)= (phoneme+possible_phonem)./2 ;
                end
                if debug
                    fprintf('Digit %d Sample %d \n', ... 
                        digit, sample_numberr)
                    plotPhonemes(phonemes)
                    keyboard;
                end
            end
        end
        phonemes = [phonemes, digit_phonemes];
    end
    [~, phonemes] = computeKmeans(phonemes', N_phonemes);
    phonemes = phonemes';
end

function phonemes = plotPhonemesSorted(phonemes, method)
    % PLOTPHONEMES sorts and plots the phonemes dictionary using tsne
    % dimensionality reduction. The sorting order is descendent by distance to
    % the tsne output origin
    N_phonemes = size(phonemes, 2);
    tsne_phonemes = tsne(phonemes','Algorithm','exact','Distance','chebychev');
    [~, sort_index] = sort(sqrt(sum(tsne_phonemes.^2, 2)), 'descend');
    tsne_phonemes = tsne_phonemes(sort_index, :);
    phonemes = phonemes(:, sort_index);
    figure(1)
    gscatter( ...
        tsne_phonemes(:,1), ...
        tsne_phonemes(:,2), ...
        1:N_phonemes, ...
        jet(N_phonemes) ...
        );
    set(gca,'XTick',[], 'YTick', [])
    title(sprintf('Phonems dictionary using %s', method))
end