function test(digit, sample_numbers)
% TEST Visually shows the sequence of phonemes of determined audio signals
% using the workspace phonemes dictionary. If TRAIN hasn't ben run previously,
% the phonemes dictionary will be loaded from "phonemes.mat"
% 
%   TEST(digit, sample_numbers)
% 
% INPUT
% 
% digit:            Digit pronounced in the testing audio samples.
% 
% sample_numbers:   Vector of sample sets to use as testing audio samples.
%                   The dataset contains 5 different samples, using therefore
%                   [1:5] as input will result in plotting all available
%                   samples of the same digit.
% 
% See also TRAIN

%% Initialization
% Get trainned phonemes
if evalin( 'base', 'exist(''phonemes'',''var'') == 1' )
    phonemes = evalin('base', 'phonemes');
    getPhoneme = evalin('base', 'getPhoneme');
else
    load('phonemes', 'getPhoneme', 'phonemes');
end
[N_coeffs, N_phonemes] = size(phonemes);
cmap = jet(N_phonemes);
% Parameters
frame_duration = 0.08; % seconds

%% Body
addpath('lib');
figure(2);
tiledlayout(length(sample_numbers),1);
for sample_number=sample_numbers
    % Process Sample
    [y, Fs, ~, t] = readsample(digit, sample_number); % Original signal
    y = preEmphasis(y); % Clean signal

    % Plot playable signal
    nexttile;
    plotaudio(y, Fs, 'Color', 'w'); 

    % Compute MFCC of the signal
    frame_length = round(frame_duration*Fs); 
    [coeffs, loc] = computeMFCC(y, Fs, N_coeffs, frame_length);
    loc = loc';

    % Check with the dictionary
    release(getPhoneme); % Avoid error using VectorQuantizerEncoder
    idx = getPhoneme(coeffs', phonemes);

    % Clean the signal clasification and build the sequence of phonemes
    [sequence, phoneme_limits] = removeConsecutiveDuplicates(idx);
    phoneme_limits = [ ... 
        [sequence sequence(end)]; ... phonem index
        [1 loc(phoneme_limits)]; ... start on signal
        [loc(phoneme_limits) loc(end)] ... end on signal
        ];

    % Plot the sequence of phonemes, it should be similar between different
    % samples of the same digit
    hold on
    for k=1:size(phoneme_limits,2)
        p = phoneme_limits(:, k);
        phonem_idx = p(1); start_ = p(2); end_ = p(3);
        plot(t(start_:end_), y(start_:end_), 'Color', cmap(phonem_idx+1, :));
        xline(t(end_), '--');
        text((t(start_)+t(end_))/2, 0.8, num2str(phonem_idx), ... 
            'HorizontalAlignment', 'center');
    end
    xlim([t(1), t(end)])
    xlabel('Time (s)');
    title(sprintf('Digit: %d Sample: %d', digit, sample_number))
    hold off
end