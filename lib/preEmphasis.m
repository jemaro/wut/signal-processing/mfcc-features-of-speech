function y = preEmphasis(y,phi)
% PREEMPHASIS Filters a given signal using pre-emphasis, Savitzky-Golay and
% moving average filters consecutively. It also rescales the output signal in
% the range [-1, 1]. It's purpose is to preprocess a given signal in a way that
% the speech recognition gives more robust results
    if ~exist('phi', 'var')
        phi=0.95;
    end

    % Premphasis filter
    y = [y; 0] - circshift([y; 0], 1)*phi;
    y = y(1:end-1);
    % Savitzky-Golay filtering
    windowWidth = 11;
    polynomialOrder = 3;
    y = sgolayfilt(y, polynomialOrder, windowWidth);
    % Moving average filtering
    B = 1/100*ones(100,1); 
    y= filter(B,1,y);
    % Rescale the signal
    r = -max(y)/min(y);
    y = rescale(y, max(-1, -1/r), min(1, r));
    % Crop silence
    % [yupper,ylower] = envelope(y);
    % y((yupper-ylower)<0.042) = [];
end

