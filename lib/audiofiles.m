function filenames = audiofiles(folder, wildcard)
    % filenames = AUDIOFILES(folder, wildcard)
    % AUDIOFILES returns a cell array with all the file paths that match the
    % wildcard in the designed folder. If no wildcard is provided '*.wav' will
    % be used. Which matches all 'wav' files of the folder.
    if ~exist('wildcard', 'var')
        wildcard = '*.wav';
    end
    filenames = dir([pwd '/' folder '/' wildcard]);
    filenames = strcat({filenames.folder}, {'/'}, {filenames.name});
end