function [y, varargout] = readsample(digit, sample_number)
    % [y, Fs, L, t, f, s, T] = READSAMPLE(digit, sample_number)
    % Samples are named with the following convention:
    % [sample_number][gender][digit].wav
    % being the gender optional.
    % READSAMPLE reads a sample from "allsamples" folder given the digit and
    % the sample_number. If not sample_number is provided, 1 will be used
    % Output:
    % y: Signal
    % Fs: Sample rate
    % L: Signal length
    % t: Time base vector that corresponds with the signal
    % f: Frequency base
    % s: Duration of the signal
    % T: Sampling step [s]
    if ~exist('sample_number', 'var')
        sample_number = 1;
    end
    filenames = audiofiles(...
        'allsamples', ... 
        [num2str(sample_number) '*' num2str(digit) '.wav']... 
        );
    if length(filenames) > 1
        warning(... 
        'There is more than one sample with the same sample number and digit.')
    end
    [y, Fs] = audioread(filenames{1});
    % Convert mono to stereo
    if (size(y, 2)==2)
        y = mean(y,2);
    end
    % Build varargout
    if nargout > 1
        varargout{1} = Fs;
        if nargout > 2
            L = length(y);
            varargout{2} = L;
            if nargout > 3
                s = L/Fs;
                t = linspace(0,s,L);
                varargout{3} = t;
                if nargout > 4
                    f = 0:1/s:Fs/2;
                    varargout{4} = f;
                    varargout{5} = s;
                    if nargout > 6
                        varargout{6} = 1/Fs;                       
                    end
                end
            end
        end
    end