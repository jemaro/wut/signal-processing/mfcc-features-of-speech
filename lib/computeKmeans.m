function [idx, C] = computeKmeans(coeffs, clusterNum)
% COMPUTEKMEANS Wraps the built in function "kmeans" with some tuned parameters
%   [idx, C] = computeKmeans(coeffs, clusterNum)
    opts = statset('Display','final');
    [idx, C] = kmeans(coeffs, clusterNum, ...
    'Distance', 'sqeuclidean', ... 
    'MaxIter', 1000,...
    'Replicates', 5, ... 
    'Options', opts ... 
    );