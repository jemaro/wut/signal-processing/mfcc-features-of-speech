function [coeffs, loc] = computeMFCC(y, Fs, N_coeffs, frame_length)
% COPMPUTEMFCC Computes the mfcc coefficients, returning also the location of
% samples corresponding to each window of data.
%
%   [coeffs, loc] = COMPUTEMFCC(y, N_coeffs, frame_length)
%
% INPUT
%
% y:            Signal
%
% Fs:           Signal sampling rate
%
% N_coeffs:     Number of MFCCs to compute for each frame. size(coeffs, 2)
%
% frame_length: Number of signal samples included in each frame
%
% OUTPUT
%
% coeffs:       (matrix | 3-D array) Mel frequency cepstral coefficients 
%
% loc:          (vector) Location of the last sample in each input frame
%
%   See also MFCC 
    [coeffs, ~, ~, loc] = mfcc(y, Fs, ... 
        'NumCoeffs', N_coeffs, ... 
        'LogEnergy', 'Ignore', ... 
        "OverlapLength", round(frame_length/2), ... 
        'Window', hamming(frame_length, "periodic") ... 
        );
    
    % Sinusoidal lifetring of the coefficients will give less weight to
    % higher coefficients improving the speech recognition results
    D = N_coeffs; % dimensionality of the feature space (number of MFCC coefficients)
    w = 1 + D/2*sin(pi/D*(1:N_coeffs)); % liftering weights vector
    % Apply liftering weights
    W = repmat(w, size(coeffs, 1), 1); % liftering weights matrix
    coeffs = coeffs.*W; 
   