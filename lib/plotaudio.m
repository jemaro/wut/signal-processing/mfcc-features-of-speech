function ax = plotaudio(wave, Fs, varargin)
    % audioplayer object initialization
    playwave=audioplayer(wave, Fs);

    % Plot
    plot( ... 
        linspace(0,playwave.TotalSamples/Fs,playwave.TotalSamples), ... 
        wave, ... 
        varargin{:} ...
        );
    ax = gca;
    set(ax, 'userdata', 0);
    tb = axtoolbar(ax, {'zoomin', 'zoomout', 'restoreview', 'pan'});

    % Play control
    axtoolbarbtn(tb, 'push', ... 
        'Icon', 'playIcon.png', ... 
        'Tooltip', 'Play', ...
        'ButtonPushedFcn', {@playCallback, ax, playwave} ...
        );

    % Mark part control
    axtoolbarbtn(tb, 'push', ... 
        'Icon', 'markPartIcon.png', ... 
        'Tooltip', 'Mark part', ...
        'ButtonPushedFcn', {@markPartCallback, ax} ...
        );

    %figure and audioplayer object initialization
    playwave.TimerFcn = {@showMarker, ax, playwave};
    playwave.TimerPeriod = 0.002;
    playwave.StopFcn={@setStopState, ax};
end

%functions definition
function setStopState(~,~, ax)
    set(ax, 'userdata', 0);
end

function []=showMarker(~,~,ax, playwave)
    samp=get(playwave,'CurrentSample')/get(playwave,'SampleRate');
    marker = findobj(ax,'type','stem','color','green');
    delete(marker)
    yl = ylim(ax); hold(ax, 'on');
    stem(ax, [samp samp], [-1 1], ... 
        'linewidth', 1.5, ... 
        'marker', 'none', ... 
        'color', 'green' ... 
        );
    drawnow;
    ylim(ax, yl); hold(ax, 'off');
end

function playCallback(~, ~, ax, playwave)
    if(get(ax,'userdata')==0) 
        marker=findobj(ax, 'type','stem','color','yellow');
        if(size(marker,1)==2)
            fs=get(playwave,'SampleRate');
            xX=round(sort(fs*[marker(1).XData(1) marker(2).XData(1)]));
            play(playwave,[xX(1), xX(2)]);
        else
            resume(playwave); 
        end
        set(ax,'userdata',1);
    elseif(get(ax,'userdata')==1) 
        pause(playwave);
        set(ax,'userdata',0);
    end
end

function []=markPartCallback(~, ~, ax)
    marking=ginput(2);
    yl = ylim(ax); hold(ax, 'on');
    marker=findobj(ax, 'type','stem','color','yellow');
    % markerText=findobj(ax, 'type','text','color','red');
    delete(marker);
    % delete(markerText);
    stem(ax, [marking(1) marking(1)], [-1 1], ... 
        'linewidth', 1.5, 'marker', 'none', 'color', 'yellow');
    stem(ax, [marking(2) marking(2)], [-1 1], ... 
        'linewidth', 1.5, 'marker', 'none', 'color', 'yellow');
    % text(ax, marking(1), 0.9, num2str(round(marking(1),2)), 'color', 'red');
    % text(ax, marking(2), 0.9, num2str(round(marking(2),2)), 'color', 'red');
    ylim(ax, yl); hold(ax, 'off');
end