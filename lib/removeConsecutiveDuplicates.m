function [NoDuplic, varargout] = removeConsecutiveDuplicates(vector)
    % NoDuplic: row vector of not duplicate occurrences of vector
    vectorSize = size(vector);
    if vectorSize(1) > 1 % If input is a column vector
        LV = [false; vector(2:end) == vector(1:end-1)];
    else
        LV = [false vector(2:end) == vector(1:end-1)];
    end
    NoDuplic = vector(~LV);
    if nargout > 1
        idxs = [1:length(LV)];
        varargout{1} =  idxs(~LV);
    end