function [y, varargout] = readaudiofile(filepath)
% READAUDIOFILE reads an audio file given its filepath
% 
%   [y, Fs, L, t, f, s, T] = READAUDIOFILE(filepath)
% 
% OUTPUT
% y: Signal
% Fs: Sample rate
% L: Signal length
% t: Time base vector that corresponds with the signal
% f: Frequency base
% s: Duration of the signal
% T: Sampling step [s]
    [y, Fs] = audioread(filepath);
    % Convert mono to stereo
    if (size(y, 2)==2)
        y = mean(y,2);
    end
    % Build varargout
    if nargout > 1
        varargout{1} = Fs;
        if nargout > 2
            L = length(y);
            varargout{2} = L;
            if nargout > 3
                s = L/Fs;
                t = linspace(0,s,L);
                varargout{3} = t;
                if nargout > 4
                    f = 0:1/s:Fs/2;
                    varargout{4} = f;
                    varargout{5} = s;
                    if nargout > 6
                        varargout{6} = 1/Fs;                       
                    end
                end
            end
        end
    end