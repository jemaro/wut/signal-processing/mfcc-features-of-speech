function C = processSample(digit, sample_number, N_coeffs, phi, extra_clusters, frame_duration)
% PROCESSSAMPLE Reads an audio sample defined by its digit and sample_number
% and then processes it by extracting a fixed amount of mel frequency cepstrum
% coefficients cluster centers that should correspond to the phonemes found in
% the audio signal.
% 
%   C = PROCESSSAMPLE(digit, sample_number, N_coeffs, phi) Processes a single
%   sample into the number of phonemes determined by getPhonemesNum plus 2,
%   which is the default value of extra_clusters.
% 
%   C = PROCESSSAMPLE(digit, sample_number, N_coeffs, phi, extra_clusters)
% 
%   C = PROCESSSAMPLE(digit, sample_number, N_coeffs, phi, extra_clusters,
%   frame_duration) 
% 
% INPUT
% 
% digit:            Number pronounced in the audio signal.
% 
% sample_number:    Index of the sample set to use as the audio signal.
% 
% N_coeffs:         Number of MFCC to compute for each frame.
% 
% phi:              Constant for preEmphasis spectrum balancing.
% 
% extra_clusters:   Additional clusters to when clustering MFCC. It is added to
%                   the number of phonems of a determined digit. Defaults to 2.
% 
% frame_duration:   Duration in seconds of the frames for MFCC computation.
%                   Defaults to 0.08.
% 
% OUTPUT
% 
% C:                Cluster centers of Mel-frequency cepstrum coefficients of
%                   different phonems. Rows are different phonems and columns
%                   are different MFCC indexes.
% 
%   See also COMPUTEMFCC, COMPUTEKMEANS, PREEMPHASIS, GETPHONEMESNUM 
    if ~exist('extra_clusters', 'var')
        extra_clusters = 2;
    end
    if ~exist('frame_duration', 'var')
        frame_duration = 0.08; % seconds
    end
    % Process Sample
    clusterNum = getPhonemesNum(digit) + extra_clusters;
    [y, Fs] = readsample(digit, sample_number);
    frame_length = round(frame_duration*Fs); 
    y = preEmphasis(y, phi);
    [coeffs, ~] = computeMFCC(y, Fs, N_coeffs, frame_length);
    [idx, C] = computeKmeans(coeffs, clusterNum);
    % Order C in apparition order
    [~, C_order] = sort(unique(idx, 'stable'));
    C = C(C_order, :);
end
